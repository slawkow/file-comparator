package com.slawek.comparator;

import java.io.*;

public class TextFilesComparator implements FileComparator {
    @Override
    public boolean compare(File f1, File f2) throws IOException {
        boolean areTheSame = true;

        BufferedReader brFirst = getBufferedReaderForFile(f1);
        BufferedReader brSecond = getBufferedReaderForFile(f2);

        try {
            String line1 = brFirst.readLine();
            String line2 = brSecond.readLine();

            int lineCounter = 1;

            while (null != line1 && null != line2) {
                if (!line1.equals(line2)) {
                    System.out.println("Difference found in line: " + lineCounter + "\nThese lines don't match: \n" + line1 + "\n" + line2);
                    findDifferencePlace(line1, line2);
                    areTheSame = false;
                }

                line1 = brFirst.readLine();
                line2 = brSecond.readLine();
                lineCounter += 1;
            }

            System.out.println(!areTheSame ? "Files are different" : "Files are equal");
        } finally {
            // done in old way because of having problems with try-with-resources
            if (null != brFirst)
                brFirst.close();
            if (null != brSecond)
                brSecond.close();
        }

        return areTheSame;
    }

    private BufferedReader getBufferedReaderForFile(File file) {
        if (null == file)
            throw new IllegalArgumentException("Invalid file was given.");

        BufferedReader brFirst = null;
        try {
            brFirst = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException("File hasn't been found. Check if given path for file is correct.:\n" + file.getAbsolutePath());
        }

        return brFirst;
    }

    private void findDifferencePlace(String s1, String s2) {
        for (int i = 0; i < s1.length() && i < s2.length(); ++i) {
            if (s1.charAt(i) != s2.charAt(i)) {
                System.out.println("Difference at character number: " + (i + 1));
                break;
            }
        }
    }
}
