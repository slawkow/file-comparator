package com.slawek.comparator;

import java.io.File;
import java.io.IOException;

public interface FileComparator {
    boolean compare(File f1, File f2) throws IOException;
}
