package test;

import com.slawek.comparator.TextFilesComparator;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TextFilesComparatorTest {

    @Test(expected = IllegalArgumentException.class)
    public void nullFilesShouldGenerateAnException() throws IOException {
        TextFilesComparator textFilesComparator = new TextFilesComparator();

        textFilesComparator.compare(null, null);
    }

    @Test
    public void testIdenticalFiles() throws IOException {
        TextFilesComparator textFilesComparator = new TextFilesComparator();

        String pathToFile = "src/test/resources/v1.txt";

        File v1 = new File(pathToFile);
        File v2 = new File(pathToFile);

        assertTrue(textFilesComparator.compare(v1, v2));
    }

    @Test
    public void testDifferentFiles() throws IOException {
        TextFilesComparator textFilesComparator = new TextFilesComparator();

        File v1 = new File("src/test/resources/v1.txt");
        File v2 = new File("src/test/resources/v2.txt");

        assertFalse(textFilesComparator.compare(v1, v2));
    }
}
